<?php 
/*
 * TP PHP
 * Vue page 404
 *
 * Copyright 2016, Eric Dufour
 * http://techfacile.fr
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 */
	require_once(PATH_VIEWS.'header.php');
	require_once(PATH_VIEWS.'alert.php');
	require_once(PATH_VIEWS.'footer.php');
?>

