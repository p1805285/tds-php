<?php
/*
 * DS PHP
 * Vue page index - page d'accueil
 *
 * Copyright 2016, Eric Dufour
 * http://techfacile.fr
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 */
//  En tête de page
?>

<?php require_once(PATH_VIEWS.'header.php');?>

<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>

<!--  Début de la page -->
<h1><?php  echo TITRE_PAGE_ACCUEIL;?></h1>

<!--  Formulaire -->
<?php if(isset($_GET['erreur']))
{
	$tmp = choixAlert('login');
	echo '<p class=error><strong><u>'.$tmp['messageAlert'].'</u></strong></p>';
}
?>
<form action="./index.php?page=hello" method="post">
	<p>
		Login : <input type="text" name="login" autofocus>
		<input type="submit" value="Valider">
	</p>
</form>

<!--  Fin de la page -->


<!--  Pied de page -->
<?php require_once(PATH_VIEWS.'footer.php'); ?>
