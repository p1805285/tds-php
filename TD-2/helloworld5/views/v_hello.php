<?php
/*
 * DS PHP
 * Vue page index - page d'accueil
 *
 * Copyright 2016, Eric Dufour
 * http://techfacile.fr
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 */
//  En tête de page
?>
<?php require_once(PATH_VIEWS.'header.php');?>

<!--  Zone message d'alerte -->
<?php 
	echo '<p></p>';
	require_once(PATH_MODELS.$page.'.php');
	require_once(PATH_VIEWS.'alert.php');
?>

<!--  Début de la page -->
<h1><?= TITRE_PAGE_HELLO ?></h1>

<!--  Liste  -->
<?php
	
	$i = 1;
	if($nbR != 0 and $mot != "")
	{
		while($i <=$nbR)
		{
			echo '<li>'.$mot.'</li>';
			$i = $i + 1;
		}
	}
	else
	{
		header("Location: ./index.php?erreur=true");
		exit();
	}
?>

<!--  Fin de la page -->


<!--  Pied de page -->
<?php require_once(PATH_VIEWS.'footer.php'); ?>